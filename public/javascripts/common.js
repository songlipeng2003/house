$(document).ready(function() {
	/* Search */
	$('#search .button-search').bind('click', function() {
		url = '/products/search';
		 
		var filter_name = $('input[name=\'keyword\']').attr('value')
		
		if (filter_name) {
			url += '?keyword=' + encodeURIComponent(filter_name);
		}
		
		location = url;
	});
	
	$('#search input[name=\'keyword\']').keydown(function(e) {
		if (e.keyCode == 13) {
			url = '/products/search';
			 
			var filter_name = $('input[name=\'keyword\']').attr('value')
			
			if (filter_name) {
				url += '?keyword=' + encodeURIComponent(filter_name);
			}
			
			location = url;
		}
	});
	
	/* Ajax Cart */
/*	$('#cart > .heading a').bind('click', function() {
		$('#cart').addClass('active');
		
		$.ajax({
			url: 'index.php?route=checkout/cart/update',
			dataType: 'json',
			success: function(json) {
				if (json['output']) {
					$('#cart .content').html(json['output']);
				}
			}
		});			
		
		$('#cart').bind('mouseleave', function() {
			$(this).removeClass('active');
		});
	});
	*/
	$("#cart").hover(
	  function () {
	   	$('#cart .content').addClass('active');
			$.ajax({
				url: '/cart/info',
				dataType: 'json',
				success: function(json) {
					if (json['output']) {
						$('#cart .content').html(json['output']);
					}
				}
			});	
	  },
	  function () {
		  $('#cart .content').removeClass('active');
	  }
	);
	
	
	/* Mega Menu */
	$('#menu ul > li > a + div').each(function(index, element) {
		// IE6 & IE7 Fixes
		if ($.browser.msie && ($.browser.version == 7 || $.browser.version == 6)) {
			var category = $(element).find('a');
			var columns = $(element).find('ul').length;
			
			$(element).css('width', (columns * 160) + 'px');
			$(element).find('ul').css('float', 'left');
		}		
		
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();
		
		i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
		
		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// IE6 & IE7 Fixes
	if ($.browser.msie) {
		if ($.browser.version <= 6) {
			$('#column-left + #column-right + #content, #column-left + #content').css('margin-left', '240px');
			
			$('#column-right + #content').css('margin-right', '240px');
		
			$('.box-category ul li a.active + ul').css('display', 'block');	
		}
		
		if ($.browser.version <= 7) {
			$('#menu > ul > li').bind('mouseover', function() {
				$(this).addClass('active');
			});
				
			$('#menu > ul > li').bind('mouseout', function() {
				$(this).removeClass('active');
			});	
		}
	}
});

$('.success img, .warning img, .attention img, .information img').live('click', function() {
	$(this).parent().fadeOut('slow', function() {
		$(this).remove();
	});
});

function addToCart(product_id) {
	$.ajax({
		url: '/cart/add',
		type: 'get',
		data: 'id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			}
			
			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="/images/cancel.png" alt="" class="close" /></div>');
				}
			}	 
						
			if (json['success']) {
				$('#notification').html('<div class="attention" style="display: none;">' + json['success'] + '<img src="/images/cancel.png" alt="" class="close" /></div>');
				
				$('.attention').fadeIn('slow');
				
				$('#cart_total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
}
function updateCart(product_id,num) {
	$.ajax({
		url: '/cart/update',
		type: 'get',
		data: 'id=' + product_id + '&number=' + num,
		dataType: 'json',
		success: function(json) {						
			if (json['success']) {
				$('#cart_total').html(json['total']);
			}
		}
	});
}

function removeCart(key) {
	$.ajax({
		url: '/cart/remove',
		type: 'post',
		data: 'id=' + key,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
			
			if (json['output']) {
				$('#cart_total').html(json['total']);
				
				$('#cart .content').html(json['output']);
			}			
		}
	});
}

function addToFavorite(product_id) {
	$.ajax({
		url: '/favorites/add',
		type: 'get',
		data: 'id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="attention" style="display: none;">' + json['success'] + '<img src="/images/cancel.png" alt="" class="close" /></div>');
				
				$('.attention').fadeIn('slow');
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 				
			}	
		}
	});
}

function checknumber(String){
	if(trimTxt(String)==""){
		return false;
	}
	var Letters = "1234567890";
	var i;
	var c;
	for( i = 0; i < String.length; i ++ ){
		c = String.charAt( i );
		if (Letters.indexOf( c ) ==-1){
			return false;
		}
	}
	return true;
} 
function trimTxt(txt){
	return txt.replace(/(^\s*)|(\s*$)/g, "");
} 