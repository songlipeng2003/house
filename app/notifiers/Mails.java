package notifiers;

import models.User;

import org.apache.commons.mail.EmailAttachment;

import play.Play;
import play.mvc.Mailer;

public class Mails extends Mailer {
	public static void test(User user) {
		setSubject("Welcome %s", user.username);
		addRecipient(user.email);
		setFrom("Test<me@me.com>");
		EmailAttachment attachment = new EmailAttachment();
		attachment.setDescription("A pdf document");
		attachment.setPath(Play.getFile("rules.pdf").getPath());
		addAttachment(attachment);
		send(user);
	}
	
	public static void forgetpass(User user,String url){
		setSubject("Reset your account password");
		addRecipient(user.email);
		setFrom("Test<me@me.com>");
		send(user,url);
	}
}
