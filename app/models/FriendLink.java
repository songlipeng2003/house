package models;

import javax.persistence.Entity;

import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class FriendLink extends Model {
	@Required
	public String siteName;
	@Required
	@play.data.validation.URL
	public String siteURL;
	
	public Integer index;
}
