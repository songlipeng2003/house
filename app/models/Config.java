package models;

import javax.persistence.Column;
import javax.persistence.Entity;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;
import validation.Unique;

@Entity
public class Config extends Model {
	@Required
	@Unique
	@MaxSize(64)
	@Column(unique = false)
	public String key;

	@Required
	@MaxSize(64)
	public String name;

	@Required
	@MaxSize(64000)
	public String value;

	public static String get(String key) {
		Config config = find("byKey", key).first();

		return config == null ? null : config.value;
	}
}
