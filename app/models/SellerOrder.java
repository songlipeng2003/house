package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class SellerOrder extends TemporalModel {
	@ManyToOne
	public User user;

	@ManyToOne
	public Order order;

	@OneToMany(mappedBy = "sellerOrder")
	public List<OrderProduct> orderProducts;

	public String note;

	public Double totalPrice;

	public enum OrderStatus {
		ORDERED, PAYMENTED, SENDED, RECEIVED
	}
}
