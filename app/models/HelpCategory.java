package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;
import utils.StringUtils;

@Entity
public class HelpCategory extends Model {
	@MaxSize(value = 32)
	public String key;

	@Required
	@MaxSize(value = 32)
	public String name;

	@Required
	public Integer sortOrder = 0;

	@MaxSize(value = 256)
	public String content;

	@Enumerated(EnumType.ORDINAL)
	public Type type;

	@OneToMany(mappedBy = "category")
	public List<Help> helps;

	@Override
	public String toString() {
		return name;
	}

	@PrePersist
	void onPrePersist() {
		key = (key == null || key.equals("") ? StringUtils
				.toUpperCaseName(name) : key);
	}

	@PreUpdate
	void onPreUpdate() {
		key = (key == null || key.equals("") ? StringUtils
				.toUpperCaseName(name) : key);
	}

	public enum Type {
		BUYER, SELLER
	}
}
