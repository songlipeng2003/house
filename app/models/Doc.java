package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;
import play.modules.search.Field;
import play.modules.search.Indexed;
import utils.StringUtils;

@Entity
@Indexed
public class Doc extends TemporalModel {
	@MaxSize(64)
	public String key;

	@Required
	@Field
	@MaxSize(64)
	public String title;

	@Required
	@MaxSize(64000)
	@Lob
	public String content;

	@Field(tokenize = false, stored = true)
	@Enumerated(EnumType.ORDINAL)
	public Type type;

	@PrePersist
	void onPrePersist() {
		key = (key == null ? StringUtils.toUpperCaseName(title) : key);
	}

	@PreUpdate
	void onPreUpdate() {
		key = (key == null ? StringUtils.toUpperCaseName(title) : key);
	}

	public enum Type {
		BUYER, SELLER
	}
}
