package models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.data.validation.MaxSize;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class Address extends TemporalModel {
	@ManyToOne
	public User user;

	@ManyToOne
	@Required
	public Country country;

	@Required
	@MaxSize(value = 64)
	public String province;

	@Required
	@MaxSize(value = 64)
	public String city;

	@Required
	@MaxSize(value = 256)
	public String address;

	@Required
	@MaxSize(value = 32)
	public String firstName;

	@Required
	@MaxSize(value = 32)
	public String lastName;

	@Required
	@Phone
	@MaxSize(value = 32)
	public String phone;

	public static List<Address> getLastlistByUser(User user, int limit) {
		return Address.find("user=? order by updated desc", user).fetch(limit);
	}
}
