package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.data.validation.MaxSize;
import play.db.jpa.Model;

@Entity
public class OrderProduct extends Model {
	@ManyToOne
	public SellerOrder sellerOrder;

	@ManyToOne
	public Product product;

	public int number;

	public Double price;

	@MaxSize(256)
	public String note;
}
