package models;

import java.io.File;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PrePersist;

import play.Play;
import play.data.validation.Required;
import play.db.jpa.Model;
import play.libs.Files;
import play.libs.Images;

@Entity
public class ProductImage extends Model {
	@Required
	@ManyToOne
	public Product product;

	public String file;

	public String path;

	public String url;

	public Integer sort;

	@PrePersist
	void onPrePersist() {
		path = "public/img/products/original/" + file;
		url = "/img/products/original/" + file;
	}

	@PostPersist
	void onPostPersist() {
		String tmpPath = "/public/tmp/" + file;
		File tmpFile = Play.getFile(tmpPath);

		File originalFile = Play.getFile(path);
		if (!originalFile.getParentFile().exists()) {
			originalFile.getParentFile().mkdirs();
		}

		Files.copy(tmpFile, originalFile);

		File file100 = Play.getFile("public/img/products/100/" + file);
		if (!file100.getParentFile().exists()) {
			file100.getParentFile().mkdirs();
		}

		Images.resize(tmpFile, file100, 100, 100);

		File file320 = Play.getFile("public/img/products/320/" + file);
		if (!file320.getParentFile().exists()) {
			file320.getParentFile().mkdirs();
		}

		Images.resize(tmpFile, file320, 320, 320);

		// 删除临时文件
		tmpFile.deleteOnExit();
		Play.getFile("public/tmp/thumbnail-" + file).deleteOnExit();
	}

	@PostRemove
	void onPostRemove() {
		// 删除图片
		Play.getFile(path).deleteOnExit();
		Play.getFile("public/img/products/100/" + file).deleteOnExit();
		Play.getFile("public/img/products/320/" + file).deleteOnExit();
	}

	public String getPath100() {
		return "/img/products/100/" + file;
	}

	public String getPath320() {
		return "/img/products/320/" + file;
	}
	
	public ProductImage delete(){
		Product product = Product.find("byImage", this).first();
		
		if(product!=null){
			product.image = null;
			ProductImage image = ProductImage.find("product=? ORDER BY sort", product).first();
			product.image = image;
			product.save();
		}
		
		return super.delete();
	}
}
