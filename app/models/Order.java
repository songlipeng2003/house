package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import play.data.validation.MaxSize;
import play.data.validation.Phone;
import play.data.validation.Required;

@Entity
public class Order extends TemporalModel {
	@ManyToOne
	public User user;

	@OneToMany(mappedBy = "order")
	public List<SellerOrder> sellerOrders;

	// 地址信息
	@ManyToOne
	@Required
	public Country country;

	@Required
	@MaxSize(value = 64)
	public String province;

	@Required
	@MaxSize(value = 64)
	public String city;

	@Required
	@MaxSize(value = 256)
	public String address;

	@Required
	@MaxSize(value = 32)
	public String firstName;

	@Required
	@MaxSize(value = 32)
	public String lastName;

	@Required
	@Phone
	@MaxSize(value = 32)
	public String phone;

	@Enumerated(EnumType.ORDINAL)
	public OrderStatus orderStatus = OrderStatus.ORDERED;

	public Double totalPrice;

	public String note;

	public enum OrderStatus {
		ORDERED, PAYMENTED, SENDED, RECEIVED
	}

	@PrePersist
	void onPrePersist() {
	}

	@PreUpdate
	void onPreUpdate() {
	}

	/**
	 * 根据address初始化订单信息
	 * 
	 * @param addressModel
	 */
	public void initAddressinfo(Address addressModel) {
		firstName = addressModel.firstName;
		lastName = addressModel.lastName;
		address = addressModel.address;
		province = addressModel.province;
		city = addressModel.city;
		country = addressModel.country;
		phone = addressModel.phone;
	}

	public String getFullName() {
		return firstName + "" + lastName;
	}

	// public String orderStatusString(){
	// String s = null;
	// switch(orderStatus){
	// case ORDERED:s = "已下单";
	// case PAYMENTED:s = "买家已付款";
	// case SENDED:s = "卖家已发货";
	// case RECEIVED:s = "买家以收货";
	// default: s = "已处理";
	// }
	// return s;
	// }
}
