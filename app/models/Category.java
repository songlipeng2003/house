package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;
import utils.StringUtils;

@Entity
public class Category extends TemporalModel {
	public String key;

	@Required
	@MaxSize(value = 32)
	public String name;

	@Required
	public Integer sortOrder = 0;

	@ManyToOne
	public Category parent;

	@OneToMany(mappedBy = "parent")
	public List<Category> children;
	
	@Lob
	public String description;

	@PrePersist
	void onPrePersist() {
		key = StringUtils.toUpperCaseName(name);
	}

	@PreUpdate
	void onPreUpdate() {
		key = StringUtils.toUpperCaseName(name);
	}

	public static List<Category> findTop() {
		return find("parent is null").fetch();
	}

	@Override
	public String toString() {
		return name;
	}

	public List<Category> getParents() {
		List<Category> list = new ArrayList<Category>();

		Category category = this;

		while (category.parent != null) {
			list.add(0, category.parent);

			category = category.parent;
		}

		return list;
	}
}
