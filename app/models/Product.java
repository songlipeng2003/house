package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import play.data.validation.MaxSize;
import play.data.validation.Min;
import play.data.validation.Required;
import play.modules.search.Field;
import play.modules.search.Indexed;
import utils.StringUtils;

@Entity
@Indexed
public class Product extends TemporalModel {
	@ManyToOne(fetch = FetchType.EAGER)
	public Country country;

	@Required
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	public ProductImage image;

	@Required
	@ManyToOne(fetch = FetchType.EAGER)
	public Category category;

	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@OrderBy("sort")
	public List<ProductImage> images;

	@Required
	@ManyToOne(fetch = FetchType.EAGER)
	@Field
	public User user;

	@Required
	@MaxSize(value = 256)
	public String key;

	@Required
	@MaxSize(value = 256)
	@Field
	public String name;

	@MaxSize(value = 256)
	@Field
	public String summary;
	
	@Required
	@Min(value=0)
	public Integer length;

	@Required
	@Min(value=0)
	public Integer width;

	@Required
	@Min(value=0)
	public Integer height;
	
	@Min(value=0)
	public Integer high;

	@MaxSize(value = 64000)
	@Field
	@Required
	public String description;

	public Boolean isOn;

	@Required
	@Min(1)
	@Field(tokenize = false, stored = true, sortable = true)
	public Double price;

	@PrePersist
	void onPrePersist() {
		summary = StringUtils.abbreviate(description, 256);
	}

	@PreUpdate
	void onPreUpdate() {
		summary = StringUtils.abbreviate(description, 256);
	}

	public static List<Product> findShopproByUser(User user) {
		List<Product> productList = Product.find("user=?", user).fetch();
		return productList;
	}
}
