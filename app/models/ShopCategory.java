package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class ShopCategory extends Model {
	@ManyToOne
	public User user;

	@Required
	@MaxSize(value = 255)
	public String name;

	/**
	 * 查询卖家所有商铺分类信息
	 * 
	 * @param user
	 * @return
	 */
	public static List<ShopCategory> findByUser(User user) {
		List<ShopCategory> shopCategories = ShopCategory.find("user=?", user)
				.fetch();
		return shopCategories;
	}
}
