package utils;

import org.apache.commons.lang.StringEscapeUtils;

public class StringUtils {
	public static String toUpperCaseName(String name) {
		name = name.replaceAll("\\W", "_");
		name = name.replaceAll("_+", "_");
		name = name.replaceAll("^_+|_+$", "");
		name = name.toLowerCase();
		return name;
	}

	public final static String abbreviate(String str, int maxWidth) {
		str = org.apache.commons.lang.StringUtils.abbreviate(str, maxWidth);
		str = StringEscapeUtils.escapeHtml(str);
		return str;
	}
}
