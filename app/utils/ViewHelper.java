package utils;

import java.util.HashMap;
import java.util.Map;

import play.Play;
import play.mvc.Http;
import play.mvc.Scope;
import play.templates.TemplateLoader;

public class ViewHelper {
	public static String parseJsonString(String template,
			Map<String, Object> data) {
		Map<String, Object> binding = new HashMap<String, Object>();
		binding.put("session", Scope.Session.current());
		binding.put("request", Http.Request.current());
		binding.put("flash", Scope.Flash.current());
		binding.put("params", Scope.Params.current());
		binding.put("play", new Play());
		binding.putAll(data);
		String errorHtml = "";
		try {
			errorHtml = TemplateLoader.load(template).render(binding);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return errorHtml;
	}
}
