package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.mvc.Http.Request;
import play.mvc.Router;
import play.mvc.Scope;

public class Paginator<T extends Object> {
	public static final String DEFAULT_PAGE_PARAM = "page";
	public static final Integer DEFAULT_PAGE_LIMIT = 12;

	protected final int page;
	protected final int limit;
	protected final int total;
	protected final String defaultAction;
	private final Map<String, Object> viewParams;

	private List<T> records = new ArrayList<T>();

	/**
	 * with default limit=12
	 * 
	 * @param offset
	 * @param total
	 */
	public Paginator(int page, int total) {
		this(page, DEFAULT_PAGE_LIMIT, total);
	}

	/**
	 * @param page
	 * @param limit
	 * @param total
	 */
	public Paginator(int page, int limit, int total) {
		if (page == 0) {
			page = 1;
		}
		this.page = page;
		this.limit = limit;
		this.total = total;
		Request request = Request.current();
		if (request != null) {
			this.defaultAction = request.action;
		} else {
			this.defaultAction = null;
		}
		Scope.Params params = Scope.Params.current();
		this.viewParams = new HashMap<String, Object>();
		if (params != null) {
			this.viewParams.putAll(params.allSimple());
		}
		this.viewParams.remove("body");
	}

	public String getAction(int page) {
		viewParams.put(DEFAULT_PAGE_PARAM, page);
		return Router.reverse(defaultAction, viewParams).url;
	}

	public List<T> getRecords() {
		return records;
	}

	public void setRecords(List<T> records) {
		this.records = records;
	}
}
