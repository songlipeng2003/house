package helper;

import java.io.Serializable;

import models.Product;

public class ProductCart implements Serializable {
	public Long id;
	public int number = 0;
	public String note;
	private transient Product product;

	public Product getProduct() {
		if (product == null) {
			product = Product.findById(id);
		}

		return product;
	}
}
