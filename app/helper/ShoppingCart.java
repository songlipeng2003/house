package helper;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import models.Product;
import utils.ViewHelper;

/**
 * 购物车
 * 
 * @author ray
 * 
 */
public class ShoppingCart implements Serializable {
	private Map<Long, SellerCart> carts = new LinkedHashMap<Long, SellerCart>();

	/**
	 * 添加，更新产品
	 * 
	 * @param id
	 * @param number
	 */
	public void add(Product product, int number) {
		SellerCart sellerCart = carts.get(product.user.id);
		if (sellerCart == null) {
			sellerCart = new SellerCart();
			sellerCart.id = product.user.id;
		}

		sellerCart.add(product.id, number);

		carts.put(product.user.id, sellerCart);
	}

	public void update(Product product, int number) {
		SellerCart sellerCart = carts.get(product.user.id);
		if (sellerCart == null) {
			sellerCart = new SellerCart();
		}

		sellerCart.update(product.id, number);

		carts.put(product.user.id, sellerCart);
	}

	/**
	 * 从购物车删除产品
	 * 
	 * @param id
	 */
	public void remove(Product product) {
		SellerCart sellerCart = carts.get(product.user.id);
		if (sellerCart == null) {
			return;
		}

		sellerCart.remove(product.id);
		if (sellerCart.isEmpty()) {
			carts.remove(product.user.id);
		}
	}

	/**
	 * 获取购物车所有产品
	 * 
	 * @return
	 */
	public Collection<SellerCart> getAllProduct() {
		Collection<SellerCart> sellerCarts = carts.values();

		return sellerCarts;
	}

	public boolean isEmpty() {
		return carts.isEmpty();
	}

	public double getTotalPrice() {
		double totalPrice = 0;

		Collection<SellerCart> sellerCarts = carts.values();

		for (SellerCart sellerCart : sellerCarts) {
			totalPrice += sellerCart.getTotalPrice();
		}

		return totalPrice;
	}

	public int getSize() {
		int size = 0;

		Collection<SellerCart> sellerCarts = carts.values();

		for (SellerCart sellerCart : sellerCarts) {
			size += sellerCart.getSize();
		}

		return size;
	}

	public int getProductSize() {
		int size = 0;

		Collection<SellerCart> sellerCarts = carts.values();

		for (SellerCart sellerCart : sellerCarts) {
			size += sellerCart.getProductSize();
		}

		return size;
	}

	public String toHtml() {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("cart", this);

		String output = ViewHelper.parseJsonString("Cart/info.html", data);

		return output;
	}

	public String getTotalSummary() {
		StringBuffer summary = new StringBuffer();
		summary.append(getSize()).append(" item(s) - $")
				.append(getTotalPrice());

		return summary.toString();
	}
}