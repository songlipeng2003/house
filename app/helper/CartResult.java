package helper;

import java.util.HashMap;
import java.util.Map;

import utils.ViewHelper;

/**
 * 购物车Json
 * 
 * @author ray
 * 
 */
public class CartResult extends Result {
	public String output;
	public String total;
	public String redirect;
}
