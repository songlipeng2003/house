package helper;

import java.util.HashMap;
import java.util.Map;

import utils.ViewHelper;

/**
 * 购物车Json
 * 
 * @author ray
 * 
 */
public class Result {
	public boolean result;
	public String success;
	public String error;
	public String info;
}
