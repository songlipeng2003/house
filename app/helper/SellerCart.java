package helper;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import models.Product;

public class SellerCart implements Serializable {
	public Long id;
	public String note;

	private Map<Long, ProductCart> carts = new LinkedHashMap<Long, ProductCart>();

	/**
	 * 添加，更新产品
	 * 
	 * @param id
	 * @param number
	 */
	public void add(Long id, int number) {
		ProductCart productCart = carts.get(id);

		if (productCart != null) {
			productCart.number += number;
		} else {
			productCart = new ProductCart();
			productCart.id = id;
			productCart.number = number;
			carts.put(id, productCart);
		}
	}

	public void update(Long id, int number) {
		ProductCart productCart = carts.get(id);

		if (productCart != null) {
			productCart.number = number;
		} else {
			productCart = new ProductCart();
			productCart.id = id;
			productCart.number = number;
			carts.put(id, productCart);
		}
	}

	/**
	 * 从购物车删除产品
	 * 
	 * @param id
	 */
	public void remove(Long id) {
		carts.remove(id);
	}

	public boolean isEmpty() {
		return carts.isEmpty();
	}

	public double getTotalPrice() {
		double sum = 0;

		Collection<ProductCart> productCarts = carts.values();

		for (ProductCart productCart : productCarts) {
			sum += productCart.getProduct().price * productCart.number;
		}

		return sum;
	}

	public int getSize() {
		return carts.size();
	}

	public int getProductSize() {
		int size = 0;

		Collection<ProductCart> productCarts = carts.values();

		for (ProductCart productCart : productCarts) {
			size += productCart.number;
		}
		return size;
	}

	/**
	 * 获取购物车所有产品
	 * 
	 * @return
	 */
	public Collection<ProductCart> getAllProduct() {
		Collection<ProductCart> productCarts = carts.values();

		return productCarts;
	}
}
