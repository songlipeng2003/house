package controllers;

import play.*;
import play.cache.Cache;
import play.i18n.Lang;
import play.libs.Images;
import play.mvc.*;
import play.mvc.Scope.Session;

import java.util.*;

import com.mysql.jdbc.Messages;

import models.*;

public class Application extends Controller {
	public final static String SESSION_USER = "userId";
	public final static String SESSION_USERNAME = "username";
	public final static String SESSION_FULLNAME = "fullname";
	public final static String SESSION_CAPTCHA = "captcha";
	public final static String SESSION_SHOPPINGCART = "shopping_cart";
	public final static String SESSION_LOGIN_RETURE_URL = "login_return_url";
	
	@Before
	static void lang() {
		String controller = request.controller;
		
		if(controller.startsWith("seller.")){
			Lang.set("zh_CN");
		}else{
			Lang.set("en");
		}
	}

	public static void index() {
		render();
	}

	protected static User getSessionUser() {
		String userid = session.get(SESSION_USER);
		if (userid != null) {
			return User.findById(Long.parseLong(userid));
		}
		return null;
	}

	/**
	 * 创建验证码，并放入缓存中
	 */
	public static void captcha() {
		Images.Captcha captcha = Images.captcha();

		String code = captcha.getText();

		String id = Session.current().getId();

		Cache.set(id + SESSION_CAPTCHA, code, "10mn");

		renderBinary(captcha);
	}

}