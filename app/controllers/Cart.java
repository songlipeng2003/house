package controllers;

import helper.CartResult;
import helper.ProductCart;
import helper.SellerCart;
import helper.ShoppingCart;

import java.util.Collection;
import java.util.List;

import models.Address;
import models.Country;
import models.Order;
import models.OrderProduct;
import models.Product;
import models.SellerOrder;
import models.User;
import play.cache.Cache;
import play.data.validation.Valid;

/**
 * 购物车流程
 * 
 * @author ray
 * 
 */
public class Cart extends Application {

	/**
	 * 购物车页面
	 */
	public static void index() {
		ShoppingCart cart = getShoppingCart();

		render(cart);
	}

	/**
	 * cart json info
	 */
	public static void info() {
		ShoppingCart cart = getShoppingCart();

		CartResult json = new CartResult();
		json.output = cart.toHtml();
		json.total = cart.getTotalSummary();

		renderJSON(json);
	}

	/**
	 * 添加产品
	 * 
	 * @param id
	 * @param number
	 */
	public static void add(Long id, Integer number) {
		notFoundIfNull(id);
		Product product = Product.findById(id);
		notFoundIfNull(product);

		ShoppingCart cart = getShoppingCart();

		number = (number == null ? 1 : (number < 1 ? 1 : number));

		cart.add(product, number);
		saveShoppingCart(cart);

		CartResult json = new CartResult();
		json.success = "You have added to your shopping cart!";
		json.output = cart.toHtml();
		json.total = cart.getTotalSummary();

		renderJSON(json);
	}

	/**
	 * 添加，更新产品信息
	 * 
	 * @param id
	 * @param number
	 */
	public static void update(Long id, Integer number) {
		notFoundIfNull(id);
		Product product = Product.findById(id);
		notFoundIfNull(product);

		ShoppingCart cart = getShoppingCart();

		cart.update(product, number);
		saveShoppingCart(cart);

		CartResult json = new CartResult();
		json.success = "You have added to your shopping cart!";
		json.output = cart.toHtml();
		json.total = cart.getTotalSummary();

		renderJSON(json);
	}

	/**
	 * 购物车删除产品
	 * 
	 * @param id
	 */
	public static void remove(Long id) {
		notFoundIfNull(id);
		Product product = Product.findById(id);
		notFoundIfNull(product);

		ShoppingCart cart = getShoppingCart();

		cart.remove(product);
		saveShoppingCart(cart);

		CartResult json = new CartResult();
		json.success = "You have removed from your shopping cart!";
		json.output = cart.toHtml();
		json.total = cart.getTotalSummary();

		renderJSON(json);
	}

	/**
	 * order checkout
	 */
	public static void check_out() {
		ShoppingCart cart = getShoppingCart();
		if (cart.isEmpty()) {
			redirect("Cart.index");
		}
		User user = getSessionUser();
		if (user == null) {
			session.put(SESSION_LOGIN_RETURE_URL, "Cart.check_out");
			redirect("buyer.Users.login");
		}
		List<Address> addresses = Address.getLastlistByUser(user, 5);

		Order order = new Order();
		if (addresses != null && addresses.size() > 0) {
			Address lastAddress = addresses.get(0);
			order.initAddressinfo(lastAddress);
		} else {
			order.firstName = user.firstName;
			order.lastName = user.lastName;
		}

		List<Country> countrys = Country.findAll();

		render(cart, countrys, order, addresses);
	}

	/**
	 * 确认订单
	 * 
	 * @param order
	 */
	public static void confirm(@Valid Order order) {
		ShoppingCart cart = getShoppingCart();
		if (cart.isEmpty()) {
			redirect("Cart.index");
		}
		if (validation.hasErrors()) {
			List<Country> countrys = Country.findAll();
			render("@check_out", cart, countrys, order);
		}
		render(cart, order);
	}

	/**
	 * place order
	 * 
	 * @param order
	 */
	public static void order(Order order) {
		ShoppingCart cart = getShoppingCart();
		if (cart.isEmpty()) {
			redirect("Cart.index");
		}
		User user = getSessionUser();
		order.user = user;
		order.totalPrice = cart.getTotalPrice();
		order.save();

		Collection<SellerCart> sellerCarts = cart.getAllProduct();
		for (SellerCart sellerCart : sellerCarts) {
			SellerOrder sellerOrder = new SellerOrder();
			sellerOrder.order = order;
			sellerOrder.note = sellerCart.note;
			sellerOrder.user = User.findById(sellerCart.id);
			sellerOrder.totalPrice = sellerCart.getTotalPrice();
			sellerOrder.save();

			Collection<ProductCart> productCarts = sellerCart.getAllProduct();
			for (ProductCart productCart : productCarts) {
				OrderProduct orderProduct = new OrderProduct();
				orderProduct.sellerOrder = sellerOrder;
				orderProduct.product = productCart.getProduct();
				orderProduct.number = productCart.number;
				orderProduct.note = productCart.note;
				orderProduct.price = productCart.getProduct().price;

				orderProduct.save();
			}
			
		}

		removeShoppingCart();

		render(order);
	}

	private static ShoppingCart getShoppingCart() {
		ShoppingCart cart = (ShoppingCart) Cache.get(session.getId()
				+ SESSION_SHOPPINGCART);
		if (cart == null) {
			cart = new ShoppingCart();
		}

		return cart;
	}

	private static void saveShoppingCart(ShoppingCart cart) {
		Cache.set(session.getId() + SESSION_SHOPPINGCART, cart, "1d");
	}

	private static void removeShoppingCart() {
		Cache.delete(session.getId() + SESSION_SHOPPINGCART);
	}

}
