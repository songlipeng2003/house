package controllers.buyer;

import helper.Result;

import java.util.List;

import models.Favorite;
import models.Product;
import models.User;

public class Favorites extends BuyerApplication {

	/**
	 * 收藏夹列表
	 */
	public static void index(int type) {
		List<Favorite> favorites = Favorite.find("user=? order by id desc",
				getSessionUser()).fetch();
		render(favorites);
	}

	public static void add(Long id) {
		Result result = new Result();

		if (id == null) {
			renderJSON(result);
		}
		Product product = Product.findById(id);
		if (product == null) {
			renderJSON(result);
		}

		User user = getSessionUser();

		Favorite favorite = Favorite.find("byUserAndProduct", user, product).first();
		
		if (favorite != null) {
			result.error = "This product been in favorite!";
			renderJSON(result);
		}
		
		favorite = new Favorite();
		favorite.user = user;
		favorite.product = product;
		favorite.save();
		result.result = true;
		result.success = "successfully!";

		renderJSON(result);
	}

	public static void delete(Long id) {
		Favorite favorite = Favorite.findById(id);
		notFoundIfNull(favorite);
		favorite.delete();

		flash.success("This favorite Deleted  successfully!");
		index(0);
	}
}
