package controllers.buyer;

import java.util.List;

import models.Country;
import models.User;
import models.User.UserType;
import notifiers.Mails;
import controllers.Application;
import controllers.CRUD;
import play.*;
import play.cache.Cache;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.libs.Crypto;
import play.mvc.*;
import play.mvc.Scope.Session;

public class Users extends Application {

	/**
	 * 个人信息配置
	 */
	public static void setting() {
		User user = getSessionUser();
		List<Country> countrys = Country.findAll();
		render(user, countrys);
	}

	/**
	 * 保存个人信息配置
	 */
	public static void savebasesetting(String firstName, String lastName,
			Country country) {
		User user = getSessionUser();
		user.firstName = firstName;
		user.lastName = lastName;
		if (country.getId() != null) {
			Country c = Country.findById(country.getId());
			user.country = c;
		}
		user.save();
		flash.success("Save successfully");
		setting();
	}

	/**
	 * 修改密码信息
	 */
	public static void savepasssetting(@Required String oldpassword,
			@Required String password, @Required String repassword) {
		validation.equals(password, repassword)
				.message("password and re-password is not equil")
				.key("password");
		User user = getSessionUser();
		validation.equals(user.password, oldpassword)
				.message("Old password are not correct").key("oldpassword");
		renderArgs.put("show", "changepass");
		if (validation.hasErrors()) {
			render("@setting", user);
		}
		user.password = password;
		user.save();

		flash.success("Password updated");
		setting();
	}

	/**
	 * 进入注册页面
	 */
	public static void register() {
		render();
	}

	/**
	 * 用户注册
	 */
	public static void reg(@Valid User user, @Required String repassword) {
		validation.equals(user.password, repassword)
				.message("password and re-password is not equil")
				.key("user.password");

		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request
			register();
		}
		user.isAdmin = false;
		user.create();
		flash.success("Thanks for registation %s", user.username);
		render();
	}

	/**
	 * 进入登录页面
	 */
	public static void login() {
		render();
	}

	/**
	 * 登录处理
	 * 
	 * @param email
	 * @param password
	 */
	public static void signin(@Required @Email String email,
			@Required String password) {
		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request
			login();
		}
		User user = User.login(email, password, UserType.BUYER,
				request.remoteAddress);
		if (user == null) {
			flash.error("sign in failed,please check your email or password",
					"");
			params.flash(); // add http parameters to the flash scope
			login();
		}
		session.put(SESSION_USERNAME, user.username);
		session.put(SESSION_FULLNAME, user.getFullname());
		session.put(SESSION_USER, user.getId());
		flash.success("Login success,Welcome %s", user.username);
		String jumpurl = session.get(SESSION_LOGIN_RETURE_URL);
		if (jumpurl != null) {
			redirect(jumpurl);
		}
		render();
	}

	/**
	 * 进入忘记密码页面
	 */
	public static void forgetpass() {
		render();
	}

	/**
	 * 忘记密码处理
	 */
	public static void forgetpasspro(@Required @Email String email,
			@Required String code) {

		String id = Session.current().getId();
		validation.equals(code, Cache.get(id + SESSION_CAPTCHA))
				.message("Invalid code. Please type it again").key("code");
		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request
			forgetpass();
		}
		User user = User.findByEmail(email);
		if (user == null || user.userType != UserType.BUYER) {
			flash.error("Email address is incorrect.");
			params.flash(); // add http parameters to the flash scope
			forgetpass();
		}
		String url = String.format("resetpass.html?email=%s&key=%s", email,
				Crypto.passwordHash(user.password + user.lastLogin));
		Mails.forgetpass(user, url);
		render(user);
	}

	/**
	 * 忘记密码--重置密码
	 */
	public static void resetpass(@Required @Email String email,
			@Required String key) {

		if (validation.hasErrors()) {
			notFound();
		}
		User user = User.findByEmail(email);
		if (user == null
				|| user.userType != UserType.BUYER
				|| !key.equals(Crypto.passwordHash(user.password
						+ user.lastLogin))) {
			notFound();
		}
		render(email, key);
	}

	/**
	 * 忘记密码--重置密码处理
	 */
	public static void resetpasspro(@Required @Email String email,
			@Required String key, @Required String password,
			@Required String repassword) {

		validation.equals(password, repassword)
				.message("password and re-password is not equil")
				.key("password");

		if (validation.hasErrors()) {
			render("buyer/resetpass.html", email, key);
		}

		User user = User.findByEmail(email);
		if (user == null
				|| user.userType != UserType.BUYER
				|| !key.equals(Crypto.passwordHash(user.password
						+ user.lastLogin))) {
			notFound();
		}
		user.password = password;
		user.save();

		render();
	}

	/**
	 * 注销系统
	 */
	public static void logout() {
		session.clear();
		Application.index();
	}
}
