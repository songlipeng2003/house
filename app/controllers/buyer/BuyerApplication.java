package controllers.buyer;

import java.util.List;

import models.Country;
import models.Order;
import models.User;
import models.User.UserType;
import notifiers.Mails;
import play.cache.Cache;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.libs.Crypto;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Router;
import play.mvc.Scope.Session;
import utils.Paginator;
import controllers.Application;

public class BuyerApplication extends Application {
	@Before
	static void checkUser() {
		if (session.contains(SESSION_USER)) {
			User user = getSessionUser();

			if (user.userType == User.UserType.BUYER) {
				return;
			}
		} else {
			return;
		}

		flash.error(Messages.get("secure.mustLogin"));
		Users.login();
	}

	public static void index() {
		render();
	}
}
