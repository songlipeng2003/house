package controllers.buyer;

import java.util.List;

import models.Order;
import utils.Paginator;

public class Orders extends BuyerApplication {

	/**
	 * 订单管理
	 */
	public static void index() {
		List<Order> orders = Order.find("byUser", getSessionUser()).fetch();
		Paginator paginator = new Paginator(0, 10);
		render(orders, paginator);
	}
}
