package controllers;

import java.io.File;
import java.util.UUID;

import org.codehaus.groovy.tools.shell.commands.ShowCommand;

import play.Play;
import play.libs.Files;
import play.libs.Images;
import play.mvc.Controller;

public class Image extends Controller {

	public static void upload(File filedata) {
		if (filedata != null && filedata.exists()) {
			String uuid = UUID.randomUUID().toString();
			String oldFilename = filedata.getName();
			int index = oldFilename.lastIndexOf(".");
			// 必须有扩展名
			if (index > 0) {
				String ext = oldFilename.substring(index + 1);
				ext = ext.toLowerCase();

				// 必须为图片
				if (ext.equals("jpg") || ext.equals("jpeg")
						|| ext.equals("gif") || ext.equals("png")) {

					String filename = uuid + "." + ext;
					String thumbnailFilename = "thumbnail-" + uuid + "." + ext;

					Files.copy(filedata, Play.getFile("public/tmp/" + filename));

					Images.resize(filedata,
							Play.getFile("public/tmp/" + thumbnailFilename),
							100, 100);

					renderText(filename);
				}
			}
		}

		renderText("error");
	}

}