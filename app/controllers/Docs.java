package controllers;

import models.Doc;
import play.mvc.Controller;

public class Docs extends Controller {

	public static void view(String key) {
		notFoundIfNull(key);

		models.Doc doc = Doc.find("byKeyAndType", key, Doc.Type.BUYER).first();

		notFoundIfNull(doc);

		render(doc);
	}

}