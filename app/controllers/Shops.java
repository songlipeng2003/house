package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Product;
import models.Seller;
import models.Shop;
import models.ShopCategory;
import models.User;

import org.hibernate.Query;

import play.db.jpa.JPA;
import play.modules.paginate.ValuePaginator;
import utils.Paginator;

/**
 * 卖家店铺
 * 
 * @author ray
 * 
 */
public class Shops extends Application {

	/**
	 * 店铺首页
	 * 
	 * @param id
	 */
	public static void index(Long id, String order, Integer limit, Integer page) {
		Shop shop = Shop.findById(id);

		notFoundIfNull(shop);

		User user = User.findById(shop.user.id);
		Seller seller = Seller.findById(shop.user.id);

		List<ShopCategory> shopcateList = ShopCategory.findByUser(user);

		limit = (limit == null ? 20 : limit);
		limit = (limit > 100 ? 100 : limit);

		page = (page == null ? 1 : page);

		String orderBy = null;

		if (order == "price") {
			orderBy = "price";
		} else if (order == "price_desc") {
			orderBy = "price DESC";
		} else if (order == "rating") {
			orderBy = "rating";
		} else if (order == "rating_desc") {
			orderBy = "rating DESC";
		} else {
			orderBy = "updated DESC";
		}

		StringBuffer query = new StringBuffer(" user=? and isOn =true ");
		query.append(" order by ").append(orderBy);

		List<Object> params = new ArrayList<Object>();
		params.add(user);
		Integer total = (int) Product.count(query.toString(), params.toArray());
		List<Product> records = Product
				.find(query.toString(), params.toArray()).fetch(page, limit);

		//System.out.println(total+"page"+page);
		Paginator paginator = new Paginator(page,limit, total);
		paginator.setRecords(records);

		render(shop, user, seller, shopcateList, paginator, order, limit);
	}
}
