package controllers;

import java.util.List;

import models.FriendLink;
import models.Product;
import play.db.jpa.GenericModel.JPAQuery;
import play.modules.paginate.ModelPaginator;
import play.mvc.Controller;

public class FriendLinks extends Controller {
	
	public static void view() {

        List<FriendLink> links = FriendLink.find("from FriendLink order by index").fetch();
        
      //  ModelPaginator paginator = new ModelPaginator(Product.class).orderBy("index");

	//	paginator.setPageSize(10000);

		notFoundIfNull(links);
		

		render(links);
	}
	
}
