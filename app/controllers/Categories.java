package controllers;

import models.Product;
import play.modules.paginate.ModelPaginator;
import play.mvc.Controller;

public class Categories extends Controller {

	public static void view(Long id, String order, Integer limit) {
		notFoundIfNull(id);

		models.Category category = models.Category.findById(id);

		notFoundIfNull(category);

		//Integer limit = params.get("limit", Integer.class);
		limit = (limit == null ? 20 : limit);
		limit = (limit > 100 ? 100 : limit);

		//String order = params.get("order", String.class);
		
		String orderBy = null;

		if (order == "price") {
			orderBy = "price";
		} else if (order == "price_desc") {
			orderBy = "price DESC";
		} else if (order == "rating") {
			orderBy = "rating";
		} else if (order == "rating_desc") {
			orderBy = "rating DESC";
		} else {
			orderBy = "updated DESC";
		}

		ModelPaginator paginator = new ModelPaginator(Product.class,
				"category=?", category).orderBy(orderBy);

		paginator.setPageSize(limit);

		render(category, order, limit, paginator);
	}
}