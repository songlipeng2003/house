package controllers.seller;

import java.util.List;

import models.Doc;
import play.mvc.Controller;

public class Docs extends Application {
	public final static void index() {
		List<Doc> docs = Doc.find("type=?", Doc.Type.SELLER).fetch();

		render(docs);
	}

	public final static void view(String key) {
		notFoundIfNull(key);
		Doc doc = Doc.find("key=? and type=?", key, Doc.Type.SELLER).first();
		notFoundIfNull(doc);
		List<Doc> docs = Doc.find("type=?", Doc.Type.SELLER).fetch();
		render(doc, docs);
	}

}
