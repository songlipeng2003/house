package controllers.seller;

import java.util.ArrayList;
import java.util.List;

import models.Product;
import models.ProductImage;
import models.User;
import play.data.binding.Binder;
import play.i18n.Lang;
import play.i18n.Messages;
import play.modules.paginate.ModelPaginator;
import play.mvc.Before;

/**
 * 卖家产品管理
 * 
 * @author ray
 * @author thinking
 * 
 */
public class Products extends SellerApplicaion {
	@Before
	static void lang() {
		Lang.set("zh_CN");
	}
	
	/**
	 * 产品列表
	 */
	public static void index() {
		ModelPaginator paginator = new ModelPaginator(Product.class, "user=?",
				getSessionUser()).orderBy("id DESC");
		paginator.setPageSize(20);
		render(paginator);
	}

	public static void show(Long id) {
		Product product = new Product();
		if (id == null) {
			render(product);
		}

		product = Product.findById(id);
		User user = getSessionUser();
		if (product != null && !product.user.equals(user)) {
			notFoundIfNull(product);
		}
		render(product);
	}

	public static void save(Long id, List<ProductImage> images) {
		Product product = null;

		User user = getSessionUser();

		if (id == null) {
			product = new Product();
		} else {
			product = Product.findById(id);

			notFoundIfNull(product);

			if (product != null && !product.user.equals(user)) {
				notFound();
			}
		}

			if (images != null && images.size() > 0) {
			for (int i = 0; i < images.size(); i++) {
				ProductImage image = images.get(i);
				if (image != null) {
					if (image.id == null || image.id == 0) {
						image = image.create("images[" + i + "]", params);
						image.product = product;
						if (product.images == null) {
							product.images = new ArrayList<ProductImage>();
						}
						product.images.add(image);
					} else {
						image = ProductImage.findById(image.id);
						image = image.edit("images[" + i + "]", params.all());
						image.save();
					}
				}
			}

			product.image = images.get(0);
		}
		
			
		product.user = user;
		
		Binder.bind(product, "product", params.all());
		validation.valid(product);

//		if(product.image == null || product.image.count() ==0){
//			validation.addError("image", "至少上传一张图片", null);
//		}

		if (validation.hasErrors()) {
			for(play.data.validation.Error error:validation.errors()) {
	             System.out.println(error.getKey()+","+error.message());
	         }
			params.flash();
		//	flash.error(Messages.get("save.fail"));
			validation.keep();
//			render("@show(" + id + ")", 0);
//			render("@show(" + id + ")", product);
			show(null);
		}
		

		product.merge();
		
		flash.success(Messages.get("save.success"));
		index();
	}

	public static void delete(Long id) {
		Product product = Product.findById(id);
		notFoundIfNull(product);
		product.delete();
		flash.success(Messages.get("delete.success"));
		index();
	}

}
