package controllers.seller;

import java.util.List;

import models.Address;
import models.Country;
import models.Shop;
import models.ShopCategory;
import models.User;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Controller;
import utils.Paginator;

/**
 * 卖家商铺信息管理
 * 
 * @author ray
 * 
 */
public class Shopinfo extends SellerApplicaion {

	public static void index() {
		User user = getSessionUser();
		Shop shop = Shop.find("byUser", user).first();
		render(shop);
	}

	public static void save(@Valid Shop shop) {
		if (validation.hasErrors()) {
			params.flash();
			render("@index", shop);
		}
		if (shop.id == null) {
			User user = getSessionUser();
			shop.user = user;
		}
		shop.save();
		flash.success(Messages.get("save.success"));
		index();
	}
}
