package controllers.seller;

import models.Seller;
import models.User;
import models.User.UserType;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Controller;

public class Users extends Application {

	public static void register() {
		render();
	}

	public static void reg(@Valid User user, @Valid Seller seller) {
		validation.equals(user.password, user.rePassword);

		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request

			register();
		} else {
			user.userType = UserType.SELLER;
			user.create();
			seller.user = user;

			seller.create();

			flash.success("感谢你的注册 %s", user.username);
			render();
		}
	}

	public final static void authenticate(@Required @Email String email,
			@Required String password) {
		if (validation.hasErrors()) {
			params.flash();
			validation.keep();
			login();
		}
		User user = User.login(email, password, UserType.SELLER, request.host);

		if (user == null) {
			flash.error(Messages.get("secure.loginFail"));
			params.flash();
			login();
		}

		session.put(Application.SESSION_USER, user.getId());
		session.put(Application.SESSION_USERNAME, user.username);
		session.put(Application.SESSION_FULLNAME, user.getFullname());
		flash.success(Messages.get("secure.loginSuccess"));
		Index.index();
	}

	public final static void login() {
		render();
	}

	public final static void logout() {
		session.clear();
		login();
	}

	/**
	 * 个人信息配置
	 */
	public static void setting() {
		User user = getSessionUser();
		Seller seller = Seller.findById(user.id);
		render(user, seller);
	}

	/**
	 * 保存个人信息配置
	 */
	public static void save_setting(String firstName, String lastName,
			@Valid Seller seller) {
		User user = getSessionUser();
		user.firstName = firstName;
		user.lastName = lastName;
		user.save();

		Seller seller2 = Seller.findById(user.id);
		seller2.companyAddress = seller.companyAddress;
		seller2.companyName = seller.companyName;
		seller2.mobile = seller.mobile;
		seller2.phone = seller.phone;
		seller2.save();
		flash.success("Save successfully");
		setting();
	}

	public static void reset_password() {
		render();
	}

	/**
	 * 修改密码信息
	 */
	public static void save_password(@Required String oldpassword,
			@Required String password, @Required String repassword) {
		validation.equals(password, repassword)
				.message(Messages.get("user.passrepass.notequip"))
				.key("password");
		User user = getSessionUser();
		validation.equals(user.password, oldpassword)
				.message(Messages.get("user.oldpassword.notcorrect"))
				.key("oldpassword");
		if (validation.hasErrors()) {
			validation.keep();
			reset_password();
		}
		user.password = password;
		user.save();

		flash.success(Messages.get("user.pass.reset"));
		reset_password();
	}
	
	public static void reset_email() {
		render();
	}
	
	/**
	 * 修改邮箱信息
	 */
	public static void save_email(@Required String oldemail,
			@Required String email, @Required String reemail) {

		User user = Application.getSessionUser();
		validation.equals(user.email, oldemail)
				.message(Messages.get("user.oldemail.notcorrect"))
				.key("oldemail");
		
		validation.equals(email, reemail)
				.message(Messages.get("user.emailnotequal"))
				.key("reemail");
		
		if (validation.hasErrors()) {
			validation.keep();
			reset_email();
		}
		user.email = email;
		user.save();

		flash.success(Messages.get("user.email.reset"));
		reset_email();
	}
	
	
}
