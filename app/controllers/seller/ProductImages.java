package controllers.seller;

import java.util.List;

import models.Product;
import models.ProductImage;
import models.User;
import play.data.binding.Binder;
import play.i18n.Messages;
import play.modules.paginate.ModelPaginator;

/**
 * 卖家产品管理
 * 
 * @author thinking
 * 
 */
public class ProductImages extends SellerApplicaion {
	public static void delete(Long id, String file) {
		if (id != null && id > 0) {
			ProductImage productImage = ProductImage.findById(id);
			notFoundIfNull(productImage);
			productImage.delete();
		}

		renderText("success");
	}

}
