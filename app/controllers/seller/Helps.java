package controllers.seller;

import java.util.List;

import models.Help;
import models.HelpCategory;
import play.mvc.Controller;

public class Helps extends Application {
	public final static void index() {
		List<HelpCategory> categories = HelpCategory.find("type=?",
				HelpCategory.Type.SELLER).fetch();

		render(categories);
	}

	public final static void view(String key) {
		notFoundIfNull(key);
		Help help = Help.find("key=? and category.type=?", key,
				HelpCategory.Type.SELLER).first();
		notFoundIfNull(help);

		List<HelpCategory> categories = HelpCategory.find("type=?",
				HelpCategory.Type.SELLER).fetch();

		render(help, categories);
	}

}
