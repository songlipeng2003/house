package controllers.seller;

import models.User;
import play.i18n.Messages;
import play.mvc.Before;

public class SellerApplicaion extends Application {

	@Before(unless = { "register", "reg", "authenticate", "login" })
	static void checkUser() {
		if (session.contains(SellerApplicaion.SESSION_USER)) {
			User user = getSessionUser();

			if (user != null && user.userType == User.UserType.SELLER) {
				return;
			}
		}

		flash.error(Messages.get("secure.mustLogin"));
		Users.login();
	}
}