package controllers.seller;
import java.util.Iterator;

import models.Order;
import models.Order.OrderStatus;
import models.SellerOrder;
import play.modules.paginate.ModelPaginator;

public class Orders extends SellerApplicaion {

	public static void listOrdered() {

//String jpql="select o from order o, orderProduct op where op.order = o and op.product.user = ?";
//List<Order> records = Order.find(
//		jpql,getSessionUser()
//	).fetch(1, 20);

//		String jpql="select o from order o, orderProduct op where op.order = o and op.product.user = ?";
//		 Query query=JPA.em().createQuery(jpql).setParameter(0, getSessionUser());
//		query.setFirstResult(1);//设置查询结果的开始记录数
//		query.setMaxResults(10);//设查询结果的结束记录数   查询记录数5~~10的数据
//		List<Order> result=query.getResultList();
//		render(result);

		ModelPaginator paginator = new ModelPaginator(SellerOrder.class,
				"user=? and order.orderStatus=?", getSessionUser(),
				OrderStatus.ORDERED).orderBy("order.id DESC");
		paginator.setPageSize(20);
		render(paginator);
}
	
	public static void listPaymented(){
		
		ModelPaginator paginator = new ModelPaginator(SellerOrder.class,
				"user=? and order.orderStatus=?", getSessionUser(),
				OrderStatus.PAYMENTED).orderBy("order.id DESC");
		paginator.setPageSize(20);
		render(paginator);

	}


	public static void listSended() {
		ModelPaginator paginator = new ModelPaginator(SellerOrder.class,
				"user=? and order.orderStatus=?", getSessionUser(),
				OrderStatus.SENDED).orderBy("order.id DESC");
		paginator.setPageSize(20);
		render(paginator);
	}
	
	public static void listReceiveed() {
		ModelPaginator paginator = new ModelPaginator(SellerOrder.class,
				"user=? and order.orderStatus=?", getSessionUser(),
				OrderStatus.RECEIVED).orderBy("order.id DESC");
		paginator.setPageSize(20);
		render(paginator);
	}

	public static void list(OrderStatus os) {
		ModelPaginator paginator = new ModelPaginator(SellerOrder.class,
				"user=? and order.orderStatus=?", getSessionUser(), os)
				.orderBy("order.id DESC");
		paginator.setPageSize(20);
		render(paginator);
	}

	public static void show(Long id) {
		notFoundIfNull(id);
		SellerOrder sellerOrder = SellerOrder.findById(id);
		notFoundIfNull(sellerOrder);
		if (!sellerOrder.user.equals(getSessionUser())) {
			notFound();
		}
		render(sellerOrder);
	}
}
