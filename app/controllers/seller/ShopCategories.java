package controllers.seller;

import java.util.List;

import models.ShopCategory;
import play.data.validation.Valid;
import play.i18n.Messages;

/**
 * 卖家类别管理
 * 
 * @author thinking
 * 
 */
public class ShopCategories extends SellerApplicaion {

	/**
	 * 商店分类
	 */
	public static void index() {
		List<ShopCategory> shopCategories = ShopCategory.find("user=?",
				getSessionUser()).fetch();
		render(shopCategories);
	}

	public static void show(Long id) {
		ShopCategory shopCategory = new ShopCategory();
		if (id == null) {
			render();
		}

		shopCategory = ShopCategory.findById(id);
		render(shopCategory);
	}

	public static void save(@Valid ShopCategory shopCategory) {
		if (validation.hasErrors()) {
			render("@show", shopCategory);
		}
		shopCategory.user = getSessionUser();
		shopCategory.save();
		flash.success(Messages.get("save.success"));
		index();
	}

	public static void delete(Long id) {
		ShopCategory shopCategory = ShopCategory.findById(id);
		notFoundIfNull(shopCategory);
		shopCategory.delete();
		flash.success(Messages.get("delete.success"));
		index();
	}
}
