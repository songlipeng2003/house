package controllers;

import java.util.List;

import models.Category;
import models.Product;
import play.modules.paginate.ModelPaginator;
import play.mvc.Controller;

public class Products extends Controller {

	public static void view(Long id) {
		notFoundIfNull(id);

		Product product = Product.findById(id);
		notFoundIfNull(product);

		render(product);
	}

	public static void search(String keyword, String order, Integer limit) {
		notFoundIfNull(keyword);

		limit = (limit == null ? 20 : limit);
		limit = (limit > 100 ? 100 : limit);

		// String order = params.get("order", String.class);

		String orderBy = null;

		if (order == "price") {
			orderBy = "price";
		} else if (order == "price_desc") {
			orderBy = "price DESC";
		} else if (order == "rating") {
			orderBy = "rating";
		} else if (order == "rating_desc") {
			orderBy = "rating DESC";
		} else {
			orderBy = "updated DESC";
		}

		ModelPaginator paginator = new ModelPaginator(Product.class,
				"name like ?", '%'+keyword+'%').orderBy(orderBy);

		paginator.setPageSize(limit);

		render(keyword, order, limit, paginator);
	}
}