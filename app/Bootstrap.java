import play.*;
import play.jobs.*;
import play.test.*;

import models.*;

@OnApplicationStart
public class Bootstrap extends Job {

	public void doJob() {
		// Check if the database is empty
		if (Category.count() == 0) {
			Fixtures.loadModels("data/category.yml");
		}
		if (Doc.count() == 0) {
			Fixtures.loadModels("data/doc.yml");
		}
		if (Country.count() == 0) {
			Fixtures.loadModels("data/country.yml");
		}
		if (Config.count() == 0) {
			Fixtures.loadModels("data/config.yml");
		}
	}

}