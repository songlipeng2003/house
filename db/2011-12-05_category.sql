# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.17)
# Database: house
# Generation Time: 2011-12-05 13:14:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Category`;

CREATE TABLE `Category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sortOrder` int(11) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`),
  KEY `FK6DD211E19E470F2` (`parent_id`),
  CONSTRAINT `FK6DD211E19E470F2` FOREIGN KEY (`parent_id`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;

INSERT INTO `Category` (`id`, `key`, `name`, `sortOrder`, `parent_id`, `created`, `updated`, `description`)
VALUES
	(1,'home_furniture','Home Furniture',0,NULL,'2011-12-05 12:13:56','2011-12-05 12:13:56',NULL),
	(2,'outdoor_furniture','Outdoor Furniture',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(3,'commercial_furniture','Commercial Furniture',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(4,'parts_accessories','Parts & Accessories',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(5,'machinery_parts','Machinery & Parts',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(6,'materials','Materials',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(7,'accessories','Accessories',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(8,'european_furniture','European furniture',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(9,'chinese_furniture','Chinese Furniture',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(10,'classical_furniture','Classical Furniture',0,NULL,'2011-12-05 12:13:57','2011-12-05 12:13:57',NULL),
	(11,'children_furniture','Children Furniture',0,1,'2011-12-05 12:16:08','2011-12-05 12:16:08',NULL),
	(12,'home_furniture','Home Furniture',0,1,'2011-12-05 12:16:24','2011-12-05 12:16:24',NULL),
	(13,'baby_furniture','Baby Furniture',0,1,'2011-12-05 12:16:35','2011-12-05 12:16:35',NULL),
	(14,'awnings','Awnings',0,2,'2011-12-05 12:18:39','2011-12-05 12:18:39',NULL),
	(15,'beach_chairs','Beach Chairs',0,2,'2011-12-05 12:19:48','2011-12-05 12:19:48',NULL),
	(16,'garden_chairs','Garden Chairs',0,2,'2011-12-05 12:20:02','2011-12-05 12:20:02',NULL),
	(17,'garden_sets','Garden Sets',0,2,'2011-12-05 12:20:11','2011-12-05 12:20:11',NULL),
	(18,'garden_sofas','Garden Sofas',0,2,'2011-12-05 12:20:22','2011-12-05 12:20:22',NULL),
	(19,'gazebos','Gazebos',0,2,'2011-12-05 12:20:34','2011-12-05 12:20:34',NULL),
	(20,'hammocks','Hammocks',0,2,'2011-12-05 12:20:46','2011-12-05 12:20:46',NULL),
	(21,'outdoor_tables','Outdoor Tables',0,2,'2011-12-05 12:21:00','2011-12-05 12:21:00',NULL),
	(22,'patio_benches','Patio Benches',0,2,'2011-12-05 12:21:24','2011-12-05 12:21:24',NULL),
	(23,'patio_swings','Patio Swings',0,2,'2011-12-05 12:21:34','2011-12-05 12:21:34',NULL),
	(24,'patio_umbrellas_bases','Patio Umbrellas & Bases',0,2,'2011-12-05 12:21:45','2011-12-05 12:21:45',NULL),
	(25,'sun_loungers','Sun Loungers',0,2,'2011-12-05 12:21:54','2011-12-05 12:21:54',NULL),
	(26,'other_outdoor_furniture','Other Outdoor Furniture',0,2,'2011-12-05 12:22:05','2011-12-05 12:22:05',NULL),
	(27,'bar_furniture','Bar Furniture',0,3,'2011-12-05 12:24:13','2011-12-05 12:24:13',NULL),
	(28,'hospital_furniture','Hospital Furniture',0,3,'2011-12-05 12:24:24','2011-12-05 12:24:24',NULL),
	(29,'hotel_furniture','Hotel Furniture',0,3,'2011-12-05 12:24:35','2011-12-05 12:24:35',NULL),
	(30,'laboratory_furniture','Laboratory Furniture',0,3,'2011-12-05 12:24:47','2011-12-05 12:24:47',NULL),
	(31,'library_furniture','Library Furniture',0,3,'2011-12-05 12:25:03','2011-12-05 12:25:03',NULL),
	(32,'office_furniture','Office Furniture',0,3,'2011-12-05 12:25:17','2011-12-05 12:25:17',NULL),
	(33,'restaurant_furniture','Restaurant Furniture',0,3,'2011-12-05 12:25:28','2011-12-05 12:25:28',NULL),
	(34,'salon_furniture','Salon Furniture',0,3,'2011-12-05 12:25:40','2011-12-05 12:25:40',NULL),
	(35,'school_furniture','School Furniture',0,3,'2011-12-05 12:25:55','2011-12-05 12:25:55',NULL),
	(36,'theater_furniture','Theater Furniture',0,3,'2011-12-05 12:26:04','2011-12-05 12:26:04',NULL),
	(37,'waiting_chairs','Waiting Chairs',0,3,'2011-12-05 12:26:18','2011-12-05 12:26:18',NULL),
	(38,'other_commercial_furniture','Other Commercial Furniture',0,3,'2011-12-05 12:26:30','2011-12-05 12:26:30',NULL),
	(39,'children_beds','Children Beds',0,11,'2011-12-05 12:29:40','2011-12-05 12:29:40',NULL),
	(40,'children_cabinets','Children Cabinets',0,11,'2011-12-05 12:29:53','2011-12-05 12:29:53',NULL),
	(41,'children_chairs','Children Chairs',0,11,'2011-12-05 12:30:06','2011-12-05 12:30:06',NULL),
	(42,'children_furniture_sets','Children Furniture Sets',0,11,'2011-12-05 12:30:29','2011-12-05 12:30:29',NULL),
	(43,'children_tables','Children Tables',0,11,'2011-12-05 12:30:40','2011-12-05 12:30:40',NULL),
	(44,'other_children_furniture','Other Children Furniture',0,11,'2011-12-05 12:30:53','2011-12-05 12:30:53',NULL),
	(45,'bathroom_furniture','Bathroom Furniture',0,12,'2011-12-05 14:27:02','2011-12-05 14:27:02',NULL),
	(46,'bedroom_furniture','Bedroom Furniture',0,12,'2011-12-05 14:27:16','2011-12-05 14:27:16',NULL),
	(47,'dining_room_furniture','Dining Room Furniture',0,12,'2011-12-05 14:27:37','2011-12-05 14:27:37',NULL),
	(48,'kitchen_furniture','Kitchen Furniture',0,12,'2011-12-05 14:27:49','2011-12-05 14:27:49',NULL),
	(49,'living_room_furniture','Living Room Furniture',0,12,'2011-12-05 14:28:02','2011-12-05 14:28:02',NULL),
	(50,'other_home_furniture','Other Home Furniture',0,12,'2011-12-05 14:28:16','2011-12-05 14:28:16',NULL),
	(51,'baby_chairs','Baby Chairs',0,13,'2011-12-05 14:28:46','2011-12-05 14:28:46',NULL),
	(52,'baby_cribs','Baby Cribs',0,13,'2011-12-05 14:29:08','2011-12-05 14:29:08',NULL),
	(53,'baby_playpens','Baby Playpens',0,13,'2011-12-05 14:29:24','2011-12-05 14:29:24',NULL),
	(54,'her_baby_furniture','her Baby Furniture',0,13,'2011-12-05 14:29:41','2011-12-05 14:29:41',NULL);

/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
